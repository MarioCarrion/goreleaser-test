package main

import (
	"fmt"
)

var (
	version = "dev"
	commit  = "none"
	date    = "unknown"
)

func main() {
	fmt.Printf("BINARY1: version %s, commit %s, date %s\n", version, commit, date)
}
